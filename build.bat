@echo off
cls

set DRIVE_LETTER=%1:
set PATH=%DRIVE_LETTER%\Java\bin;%DRIVE_LETTER%\Java\ant-1.9.9\bin;c:\Windows
set CLASSPATH=./src;%DRIVE_LETTER%/Java/junit-4.12/junit-4.12.jar
set CLASSPATH=./src;%DRIVE_LETTER%/Java/junit-4.12/hamcrest-core-1.3.jar
::is this correct?
::ant build -Ddrive-letter=%DRIVE_LETTER%
ant test -Ddrive-letter=%DRIVE_LETTER%
::ant run -Ddrive-letter=%DRIVE_LETTER%